package cl.duoc.robinsonpicunche_prueba1.entidad;

import java.util.ArrayList;

/**
 * Created by DUOC on 22-04-2017.
 */

public class BaseDeDatos {

    private static ArrayList<Usuario> values = new ArrayList<>();
    public static void agregarUsuario(Usuario usuario){
        values.add(usuario);
    }

    public static ArrayList<Usuario> obtieneListadoUsuarios(){
        return values;
    }
}
