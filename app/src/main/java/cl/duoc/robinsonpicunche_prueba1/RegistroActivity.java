package cl.duoc.robinsonpicunche_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import cl.duoc.robinsonpicunche_prueba1.entidad.Usuario;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etUsuarioRegistro, etClave1, etClave2;
    Button btnCrearUsuario, btnVolverLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        etUsuarioRegistro=(EditText) findViewById(R.id.etUsuarioRegistro);
        etClave1=(EditText) findViewById(R.id.etClave1);
        etClave2=(EditText) findViewById(R.id.etClave2);

        btnCrearUsuario=(Button) findViewById(R.id.btnCrearUsuario);
        btnVolverLogin=(Button) findViewById(R.id.btnVolverLogin);

        btnCrearUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nomUsuario = etUsuarioRegistro.getText().toString();
                if (etClave1 == etClave2){
                    String clave = etClave1.getText().toString();
                }
                Usuario usu  = new Usuario();
            }
        });
        btnVolverLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId()== R.id.btnVolverLogin){
            Intent intent = new Intent(RegistroActivity.this, LoginActivity.class);
            startActivity(intent);
        }
    }
}
