package cl.duoc.robinsonpicunche_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etUsuario, etPassword;
    private Button btnEntrar, btnRegistrarse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsuario=(EditText) findViewById(R.id.etUsuario);
        etPassword=(EditText) findViewById(R.id.etPassword);
        btnEntrar=(Button) findViewById(R.id.btnEntrar);
        btnRegistrarse=(Button) findViewById(R.id.btnRegistrarse);

        btnEntrar.setOnClickListener(this);
        btnRegistrarse.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId()== R.id.btnRegistrarse){
            Intent intent = new Intent(LoginActivity.this, RegistroActivity.class);
            startActivity(intent);
        }
    }
}
